#include <linux/module.h>
#include <linux/pci.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/sched.h>
#include <asm/current.h>
#include <linux/idr.h>
#include <linux/kernel.h>
#include <linux/spinlock.h>
#include <linux/slab.h>
#include <linux/list.h>

#include "vintage2d.h"
#include "v2d_ioctl.h"

#define bool short
#define true 1
#define false 0

MODULE_AUTHOR("Marcin Dominiak");
MODULE_DESCRIPTION("Driver for vintage2d device");
MODULE_LICENSE("GPL");

struct class *vintage_class = NULL;
struct pci_driver pci_struct;
struct pci_device_id device;
int no_devices;
int mjr;
void __iomem *bar0;    

DEFINE_SPINLOCK(users_lock);

struct context
{
    struct list_head list;
    uid_t uid;
    int counter;
    bool initialized;
};
struct context users;

bool uid_in_list(uid_t uid, int change)
{
    struct list_head* ptr;
    struct context* obj;

    list_for_each(ptr, &(users.list))
    {
        obj = list_entry(ptr, struct context, list);
        if (obj->uid == uid)
        {
            obj->counter += change;
            return true;
        }
    }
    return false;
}

void remove_or_dec_from_list(uid_t uid)
{
    struct list_head *ptr, *tmp;
    struct context* obj;

    list_for_each_safe(ptr, tmp, &(users.list))
    {
        obj = list_entry(ptr, struct context, list);
        obj->counter -= 1;
        if (obj->counter == 0)
        {
            list_del(ptr);
            kfree(obj);
        }
    }
}

void shutdown(struct pci_dev *dev)
{
    printk(KERN_INFO "vintage2d: shutting down, NOP\n");
}

int resume(struct pci_dev *dev)
{
    printk(KERN_INFO "vintage2d: resuming not supported, NOP\n");
    return 0;
}

int suspend(struct pci_dev *dev, pm_message_t msg)
{
    printk(KERN_INFO "vintage2d: suspending not supported, NOP\n");
    return 0;
}

void remove(struct pci_dev *dev)
{

    printk(KERN_INFO "vintage2d: removed one of vintage2d devices\n");
    pci_iounmap(dev, bar0);
    pci_release_regions(dev);
    pci_disable_device(dev);
    device_destroy(vintage_class, MKDEV(mjr, (no_devices - 1)));
    no_devices--;
}

bool is_command_correct(int32_t type, int32_t cmd, int32_t * out1, int32_t * out2)
{
    printk(KERN_DEBUG "vintage2d: received command %d\n", cmd);
    if ((type == V2D_CMD_TYPE_SRC_POS) || (type == V2D_CMD_TYPE_DST_POS))
    {
        //check if unused bits are zero
        if ((cmd & 0x80080000) != 0)
            return false;

        //set arguments are return true
        *out1 = cmd & 0x7ff00;
        *out2 = cmd & 0x7ff0000;
        //*out2 = cmd & 0xffffffffff;

        return true;
    }

    if (type == V2D_CMD_TYPE_FILL_COLOR)
    {
        if ((cmd & 0xffff0000) != 0)
            return false;

        *out1 = cmd & 0xff00;
        return true;
    }

    if ((type == V2D_CMD_TYPE_DO_FILL) || (type == V2D_CMD_TYPE_DO_BLIT))
    {
        if ((cmd & 0x80080000) != 0)
            return false;

        *out1 = cmd & 0x7ff00;
        *out2 = cmd & 0x7ff0000;
        return true;
    }
    return false;
}

ssize_t vintage2d_write(struct file * filp, const char __user * buff, size_t count, loff_t * offp)
{
    int32_t command, command_type, height, width, coord1, coord2, color;
    int i;
    ssize_t written;

    printk(KERN_DEBUG "vintage2d: write called\n");

    written = 0;

    if (count % (32 / 8) != 0)
    {
        printk(KERN_ERR "vintage2d: write wrote size (%d) is not a multiple of 32\n", count);   
        return -EINVAL;
    }

    for (i = 0; i < (count / (32 / 8)); i += 4)
    {
        command = 0;
        command |= buff[i];
        command |= (buff[i + 1] << 8);
        command |= (buff[i + 2] << 16);
        command |= (buff[i + 3] << 24);

        printk(KERN_DEBUG "vintage2d: command received: %d\n", command);
        command_type = V2D_CMD_TYPE(command);
        if (command_type == V2D_CMD_TYPE_SRC_POS)
        {
            printk(KERN_DEBUG "vintage2d: write v2d_src_pos\n");
            if (is_command_correct(command_type, command, &coord1, &coord2) == false)
                return written;
        }
        else if (command_type == V2D_CMD_TYPE_DST_POS)
        {
            printk(KERN_DEBUG "vintage2d: write v2d_dst_pos\n");
            if (is_command_correct(command_type, command, &coord1, &coord2) == false)
                return written;

        }
        else if (command_type == V2D_CMD_TYPE_FILL_COLOR)
        {
            printk(KERN_DEBUG "vintage2d: write v2d_fill_color\n");
            if (is_command_correct(command_type, command, &color, NULL) == false)
                return written;
        }
        else if (command_type == V2D_CMD_TYPE_DO_BLIT)
        {
            printk(KERN_DEBUG "vintage2d: write do_blit\n");
            if (is_command_correct(command_type, command, &width, &height) == false)
                return written;

        }
        else if (command_type == V2D_CMD_TYPE_DO_FILL)
        {
            printk(KERN_DEBUG "vintage2d: write do_fill\n");
            if (is_command_correct(command_type, command, &width, &height) == false)
                return written;

        }
        else
        {
            printk(KERN_ERR "vintage2d: write received unknown command\n");
            return -EINVAL;
        }
            
    }

    return count;
}

long vintage2d_unlocked_ioctl(struct file * fil, unsigned int ui, unsigned long ul)
{
    printk(KERN_DEBUG "vintage2d: unlocked_ioctl called\n");

    return 0;
}

int vintage2d_fsync(struct file * fil, loff_t loff, loff_t loff2, int datasync)
{
    printk(KERN_DEBUG "vintage2d: fsync called\n");

    return 0;
}

long vintage2d_compat_ioctl(struct file * fil, unsigned int ui, unsigned long ul)
{
    printk(KERN_DEBUG "vintage2d: compat_ioctl called\n");

    return 0;

}

int vintage2d_mmap(struct file * fil, struct vm_area_struct * vm_area)
{
    printk(KERN_DEBUG "vintage2d: mmap called\n");

    return 0;
}

int vintage2d_open(struct inode * inod, struct file * fil)
{
    uid_t user;
    struct context* new_user;

    printk(KERN_DEBUG "vintage2d: open called\n");

    user = get_current()->cred->uid.val; 
    
    spin_lock(&users_lock);
    if (uid_in_list(user, 1))
    {
        printk(KERN_INFO "vintage2d: user %d allready has context\n", user);
        spin_unlock(&users_lock);
        return 0; //user allready has his context
    }
    spin_unlock(&users_lock);

    new_user = kmalloc(sizeof(struct context), GFP_KERNEL);
    if (new_user == NULL)
    {
        printk(KERN_ERR "vintage2d: kmalloc failed\n");
        return -ENOSPC;
    }

    new_user->uid = user;
    new_user->initialized = false;
    new_user->counter = 1;
    INIT_LIST_HEAD(&new_user->list);

    spin_lock(&users_lock);
    list_add(&new_user->list, &users.list);
    spin_unlock(&users_lock);

    return 0;
}

int vintage2d_release(struct inode * inod, struct file * fil)
{
    uid_t user;

    printk(KERN_DEBUG "vintage2d: release called\n");
    user = get_current()->cred->uid.val;
    spin_lock(&users_lock);
    if (uid_in_list(user, 0))
        remove_or_dec_from_list(user);
    spin_unlock(&users_lock);
    return 0;
}

static struct file_operations f_ops = {
    .owner = THIS_MODULE,
    .fsync = &vintage2d_fsync,
    .write = &vintage2d_write,
    .unlocked_ioctl = &vintage2d_unlocked_ioctl,
    .compat_ioctl = &vintage2d_compat_ioctl,
    .mmap = &vintage2d_mmap,
    .open = &vintage2d_open,
    .release = &vintage2d_release
};

int probe(struct pci_dev *dev, const struct pci_device_id *id)
{
    struct device *device;
    char full_name[8];
    int ret;
    struct class;

    full_name[0] = 'v';
    full_name[1] = '2';
    full_name[2] = 'd';

    printk(KERN_DEBUG "vintage2d: entered probe\n");
    
    if (no_devices >= 255)
    {
        printk(KERN_ERR "vintage2d: too many probes (over 255). Aborting...\n");
        return -EAGAIN;
    }
    

    if (no_devices < 10)
        full_name[3] = no_devices + '0';
    else if (no_devices < 100)
    {
        full_name[4] = (no_devices % 10) + '0';
        full_name[3] = ((no_devices - (no_devices % 10)) / 10) + '0';
    }
    else
    {
        full_name[5] = (no_devices % 10) + '0';
        full_name[4] = ((no_devices - (no_devices % 10)) / 10) + '0';
        full_name[3] = ((no_devices - (no_devices % 100)) / 100) + '0';
   
    }

    full_name[6] = '\0';
    full_name[7] = '\0';

    if (mjr == 0)
    {
        ret = (mjr = register_chrdev(0, &(full_name[0]), &f_ops));

        if (ret < 0)
        {
            printk(KERN_ERR "vintage2d: register_chrdev failed, err: %d\n", ret);
            return ret;
        }
    }
    
    
    if (vintage_class == NULL)
    {
        vintage_class = class_create(THIS_MODULE, "vintage2d");
        if (IS_ERR(vintage_class))
        {
            printk(KERN_ERR "vintage2d: class_create failed\n");
            return -1; 
        }
    }

    device = device_create(vintage_class, &dev->dev, MKDEV(mjr, no_devices), NULL, "v2d%d", no_devices);

    if (IS_ERR(device))
    {
        printk(KERN_ERR "vintage2d: device_create failed\n");
        return -1;
    }

    
    ret = pci_enable_device(dev);
    if (IS_ERR_VALUE(ret))
    {
        printk(KERN_ERR "vintage2d: pci_enable_device failed\n");
        goto cleanup_device_create;
    }

    ret = pci_request_regions(dev, "vintage2drv");
    if (IS_ERR_VALUE(ret))
    {
        printk(KERN_ERR "vintage2d: pci_request_regions failed\n");
        goto cleanup_enable_device;
    }

    bar0 = pci_iomap(dev, 0, 0);
    if (IS_ERR(bar0))
    {
        printk(KERN_ERR "vintage2d: pci_iomap failed\n");
        ret = PTR_ERR(bar0);
        goto cleanup_iomap;
    } 

    no_devices++; //we added a device - no_devices now points on the next free number
    printk(KERN_DEBUG "vintage2d: probe successfull\n");
    return 0; 

    /* cleanup after error section */
cleanup_iomap:
    pci_release_regions(dev);
cleanup_enable_device:
    pci_disable_device(dev);
cleanup_device_create:
    device_destroy(vintage_class, MKDEV(mjr, no_devices));

    return ret;
}

void register_vintage2d_driver(void)
{
    //vintage2d data
    device.vendor = VINTAGE2D_VENDOR_ID; 
    device.device = VINTAGE2D_DEVICE_ID;

    //don't perform additional filtering
    device.subvendor = PCI_ANY_ID;
    device.subdevice = PCI_ANY_ID;
    device.class = 0;
    device.class_mask = 0;
    
    pci_struct.name = "vintage2drv";
    pci_struct.id_table = &device;
    pci_struct.probe = &probe;
    pci_struct.remove = &remove;
    pci_struct.suspend = &suspend;
    pci_struct.resume = &resume;
    pci_struct.shutdown = &shutdown;
    
    if (pci_register_driver(&pci_struct) != 0)
        printk(KERN_ERR "vintage2d: Failed to register vintage2d MD driver\n");
}

int vintage2d_init(void)
{
    printk(KERN_INFO "vintage2d: MD driver startup\n");
    INIT_LIST_HEAD(&(users.list));
    users.uid = -1; //fake head
    users.initialized = false;
    users.counter = -1;
    /*users = kmalloc(sizeof struct context, GFP_KERNEL);
    if (users == NULL)
    {
        printk(KERN_ERR "vintage2d: Kmalloc failed in init\n");
        return -ENOSPC;
    }*/
    no_devices = 0;
    mjr = 0;
    register_vintage2d_driver();
    printk(KERN_INFO "vintage2d: MD driver registered to PCI\n");

    return 0;
}

void vintage2d_cleanup(void)
{
    int i;

    for (i = no_devices - 1; i >= 0; i--)
        device_destroy(vintage_class, MKDEV(mjr, i));
    class_destroy(vintage_class);
    pci_unregister_driver(&pci_struct);
    printk(KERN_INFO "vintage2d: Disabled vintage2d MD driver\n");
}

module_init(vintage2d_init);
module_exit(vintage2d_cleanup);
