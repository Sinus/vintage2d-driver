
ifneq ($(KERNELRELEASE),)

obj-m := vintage2d.o

else

KDIR ?= ~marcin/zso/kernel/linux-4.1.18

default:
	$(MAKE) -C $(KDIR) M=$$PWD

install:
	$(MAKE) -C $(KDIR) M=$$PWD modules_install

clean:
	$(MAKE) -C $(KDIR) M=$$PWD clean

endif
